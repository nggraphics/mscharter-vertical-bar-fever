var browserSync    = require('browser-sync');
var concat         = require('gulp-concat');
var csso           = require('gulp-csso');
var fileinclude    = require('gulp-file-include');
var gulp           = require('gulp');
var gulpif         = require('gulp-if');
var inquirer       = require('inquirer');
var rename         = require('gulp-rename');
var rimraf         = require('gulp-rimraf');
var runSequence    = require('run-sequence');
var sass 		   = require('gulp-sass');
var smoosher       = require('gulp-smoosher');
var template       = require('gulp-template-compile');
var uglify         = require('gulp-uglify');
var useref         = require('gulp-useref');

var REWRITE_MIDDLEWARE;
var COMPILE;

function getDirectories(baseDir) {
	return fs.readdirSync(baseDir).filter(function (file) {
		return fs.statSync(baseDir + '/' + file).isDirectory();
	});
}


gulp.task('clean', function() {
	return gulp.src(['.tmp','COMPILED.html'], {
		read: false,
		cwd: './'
	}).pipe(rimraf());
});

gulp.task('compile-stylesheets', function() {
	return gulp.src('css/*', {cwd: './'})
		.pipe(sass({compass: true}))
		.pipe(gulp.dest('.tmp', {cwd: './'}))
		.pipe(browserSync.reload({stream:true}));
});

gulp.task('build-html', function() {

	return gulp.src('./html/middle.html')
		.pipe(fileinclude())
		.pipe(rename('index.html'))
		.pipe(gulp.dest('./'))
		.pipe(browserSync.reload({stream:true}));
});

gulp.task('browser-sync', function() {

	// watch for changes to html
	gulp.watch([
		'./html/middle.html',
		'./html/*'
	], ['build-html']);

	// watch for changes to scss
	gulp.watch([
		'./css/*'
	], ['compile-stylesheets']);

	// watch for changes to templates
	gulp.watch([
		'./js/*'
	], ['compile-templates']);

	browserSync({
		server: {
			baseDir: './',
			middleware: REWRITE_MIDDLEWARE
		},
		ghostMode: false,
		startPath: './',
		files: [
			'js/*.js',
			'.tmp/*.js',
			'.tmp/*.css'
		]
	});
});

gulp.task('build-html-prod', function() {
	return gulp.src('template-prod.html', {cwd: './'})
		.pipe(fileinclude())
		.pipe(rename('COMPILED.html'))
		.pipe(gulp.dest('./', {cwd: './'}));
});

gulp.task('minify', function() {

	var assets = useref.assets();

	return gulp.src('COMPILED.html', {cwd: './'})
		.pipe(assets)
		.pipe(gulpif('*.js', uglify()))
		.pipe(gulpif('*.css', csso(true)))
		.pipe(assets.restore())
		.pipe(useref())
		.pipe(gulp.dest('./', {cwd: './'}));
});

gulp.task('smoosher', function() {

	return gulp.src('COMPILED.html', {cwd: './'})
		.pipe(smoosher({
			cssTags: {
				begin: '<style>',
				end: '</style>'
			}
		}))
		.pipe(gulp.dest('./', {cwd: './'}));
});

function build() {

	if (COMPILE) {

		runSequence(
			'clean'
			,'compile-stylesheets'
			,'js'
			,'build-html-prod'
			,'minify'
			,'smoosher'
		);

	} else {

		runSequence(
			'clean'
			,'compile-stylesheets'
			,'js'
			,'compile-templates'
			,'build-html'
			,'browser-sync'
		);
	}

}

gulp.task('setup', function(done) {

	var prompts = [{
		type: 'confirm',
		name: 'compile',
		message: 'Do you want to compile?',
		default: false
	}];

	inquirer.prompt(prompts, function(answers) {

		COMPILE = answers.compile;
		done();
	});

});

gulp.task('default', ['setup'], function() {

	build();

});